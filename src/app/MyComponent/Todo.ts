export class Todo {
  constructor(sno: number, title: string, desc: string, active: boolean) {
    this.sno = sno;
    this.title = title;
    this.desc = desc;
    this.active = active;
  }

  private sno: number;
  private title: string;
  private desc: string;
  private active: boolean;

  public getSno(): number {
    return this.sno;
  }

  public setSno(sno: number): void {
    this.sno = sno;
  }

  public getTitle(): string {
    return this.title;
  }

  public setTitle(title: string): void {
    this.title = title;
  }

  public getDesc(): string {
    return this.desc;
  }

  public setDesc(desc: string): void {
    this.desc = desc;
  }

  public isActive(): boolean {
    return this.active;
  }

  public setActive(active: boolean): void {
    this.active = active;
  }
}
