import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Todo } from '../Todo';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;

  @Output() todoDelete: EventEmitter<Todo> = new EventEmitter();

  @Output() todoCheck: EventEmitter<Todo> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onClickTodoDeleteButton(todo: Todo) {
    console.log('on Click has been clicked for deletion');
    console.log(JSON.stringify(todo));
    console.log('Emitting Event ---->');
    this.todoDelete.emit(todo);
  }
  onClickTodoCheckButton(todo: Todo) {
    console.log('on Click has been clicked for checking');
    console.log(JSON.stringify(todo));
    todo.setActive(!todo.isActive());
    console.log('Emitting Event ---->');
    this.todoCheck.emit(todo);
  }
}
