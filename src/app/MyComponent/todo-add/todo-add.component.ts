import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoremIpsum } from 'lorem-ipsum';
import { Todo } from '../Todo';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css'],
})
export class TodoAddComponent implements OnInit {
  @Output() todoAdd: EventEmitter<Todo> = new EventEmitter();

  title: string;
  desc: string;
  lorem: LoremIpsum;
  form: FormGroup;
  formBuilder: FormBuilder;
  constructor() {
    this.formBuilder = new FormBuilder();
    this.lorem = new LoremIpsum({
      sentencesPerParagraph: {
        max: 8,
        min: 4,
      },
      wordsPerSentence: {
        max: 8,
        min: 4,
      },
    });
  }
  ngOnInit(): void {
    this.form = this.formBuilder.group({
      desc: [null, [Validators.required]],
      title: [null, Validators.required],
    });
  }

  onSubmit(formData: any) {
    console.log(formData);

    let todo = new Todo(-1111, this.title, this.desc, false);
    console.log(`Emitting Todo for Saving - ${JSON.stringify(todo)}`);
    this.todoAdd.emit(todo);
  }

  onClickTodoAutoGenerateButton() {
    this.title = this.lorem.generateSentences(1);
    this.desc = this.lorem.generateParagraphs(1);
  }
}
