import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/MyComponent/Todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
})
export class TodosComponent implements OnInit {
  todos: Todo[];
  localItems: string | null;
  constructor() {
    this.localItems = localStorage.getItem('todos');
    if (this.localItems == null) {
      this.todos = [];
    } else {
      this.todos = [];
      JSON.parse(this.localItems).forEach(
        (element: {
          sno: number;
          title: string;
          desc: string;
          active: boolean;
        }) => {
          let data = new Todo(
            element.sno,
            element.title,
            element.desc,
            element.active
          );
          this.todos.push(data);
        }
      );
    }
  }
  ngOnInit(): void {}

  deleteToDoItem(event: Todo) {
    console.log('Captured Item for deletion !!');
    console.log(JSON.stringify(event));
    let index = this.todos.indexOf(event);
    this.todos.splice(index, 1);
    localStorage.setItem('todos', JSON.stringify(this.todos));
  }

  addTodoItem(event: Todo) {
    console.log('Captured Item for Addition !!');
    console.log(JSON.stringify(event));
    let index =
      this.todos.length === 0
        ? 1
        : this.todos[this.todos.length - 1].getSno() + 1;
    event.setSno(index);
    this.todos.push(event);
    localStorage.setItem('todos', JSON.stringify(this.todos));
  }

  checkToDoItem(event: Todo) {
    console.log('Captured Item for Check !!');
    console.log(JSON.stringify(event));
    let index = this.todos.indexOf(event);
    this.todos[index] = event;
    localStorage.setItem('todos', JSON.stringify(this.todos));
  }
}
